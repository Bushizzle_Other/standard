
$(function () {

	var 	$drags = $(".rastask"),
			$drops = $(".sobir"),
			$sbros = $('.task-retry'),
			$table = $('.rasp');

	var updated = false;
	var sizes = [];
	$('.stolb').each(function(){sizes.push($(this).width())});

	var minWidth = Math.min.apply(Math, sizes);

	function checkForUpdate($el){
		setTimeout(function(){

			if(updated) {

				updated = false;
				$el.draggable("disable").removeClass('rastask-ready');
				if($el.hasClass('rastask-fake')) $el.addClass('rastask-trapped');

				if (!$('.rastask-ready').length != 1) $check.removeClass('check_blocked');
			}

		}, 300);
	}


	$drops
		.attr('data-size', 0)
		.sortable({
			connectWith: ".sobir",
			update: function(){

				var $drop = $(this),
					size = $drop.data('size');

				if(size != $drop.children().length){

					$drop
						.attr('data-size', size)
						.find('.rastask-ready')
						.width(minWidth)
						.removeClass('rastask-ready');

					updated = true;
				}
			},
			beforeStop: function(event, ui){

				if(ui.offset.top > $table.offset().top + $table.height()) {

					var $item = $(ui.item[0]),
						id = $item.data('id'),
						$parentItem = $drags.filter(function(){ return $(this).data('id') == id});


					if($item.hasClass('rastask-fake'))$parentItem.removeClass('rastask-trapped').draggable('enable');
					else {
						$parentItem.addClass('rastask-ready').draggable('enable');
						$check.addClass('check_blocked');
					}

					$item.remove();
				}
				//$sbros.addClass('showme_cell');
			},
			over: function(){
				$(this).addClass('active');
			},
			out: function(){
				$(this).removeClass('active');
			}
		});

	$drags
		.draggable({
			connectToSortable: ".sobir",
			helper: function() {
				var helper = $(this).clone();
				helper.width(minWidth);
				return helper;
			},
			cursorAt: {
				left: minWidth/6
			},
			start: function(){
				$(this).width(minWidth);
			},
			stop: function( event ) {

				checkForUpdate($(this));

			}
		})
		.not('.rastask-fake')
		.addClass('rastask-ready')
		.each(function(){
			$(this).attr('data-id', Math.round(Math.random()*100000));
		});

	$check
		.addClass('check_blocked')
		.click(function(){

		if(!$(this).hasClass('check_blocked')){

			$drops.each(function(){

				var $drop = $(this),
					$els = $drop.children(),
					correct = $drop.data('correct');

				$els.each(function(){
					var $el = $(this);

					if($el.data('val') == correct)$el.addClass('rastask-correct');
					else $el.addClass('rastask-error');
				});

			});

			if($('.rastask-error').length != 0 || $drags.not('.rastask-fake').hasClass('rastask-ready')) MMO.task.result.show(false);
			else MMO.task.result.show(true);

			//$sbros.removeClass('showme_cell');

		}

	});


	$restart.click(function(){
		$drops.html('');
		$drags.draggable('enable').removeClass('rastask-trapped').not('.rastask-fake').addClass('rastask-ready');
		$check.addClass('check_blocked');
	});

	$sbros.click(function(){
		$drops.html('');
		$drags.draggable('enable').removeClass('rastask-trapped').not('.rastask-fake').addClass('rastask-ready');
		$check.addClass('check_blocked');
	});


});



