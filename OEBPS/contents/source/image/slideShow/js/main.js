$(function(){

    function clear(c, a, b){
        while(c.indexOf(a) > 0)c = c.replace(a, b);
        return c;
    }

    $area.append('<img id="slide" src="#" alt=""/><div id="play" class="loading"></div>');

    $check.add($retry).add($restart).add($retry).add($header).add($text).remove();

    MMO.load.xml(function(){
        parseData();

        audios.forEach(function(item, i){
            audioPreload(item.file, i);
        });
    });


    var audio = new Audio(),
        $playBtn = $('#play'),
        $img = $('#slide'),
        audios = [],
        images = [],
        currentAudio = 0,
        currentImage = 0,
        currentTimeOffset = false,
        isPlaying = false,
        nextImage,
        audioPreloadCounter = 0;

    // grade

    var sumAudioLength = 0,
        percent = ['0.05','0.10','0.15','0.20','0.25','0.30','0.35','0.40', '0.45','0.50','0.55','0.60','0.65','0.70','0.75','0.80','0.85','0.90','0.95'],
        gradeReady = false;

    audio.ontimeupdate = function(){

        if(!gradeReady){
            var currentTime = 0;
            for(var i = 0; i < currentAudio;i++){currentTime+= audios[i].duration/1000;}
            currentTime+= audio.currentTime;

            console.log('upd', currentTime, sumAudioLength);

            var currentProgress = (currentTime/sumAudioLength).toFixed(2);

            if(percent.includes(currentProgress)) {
                percent.splice(percent.indexOf(currentProgress), 1);
                console.log(percent);
            }

            if(!percent.length) {
                MMO.task.result.show(true, 100, true);
                gradeReady = true;
            }
        } else {
            audio.ontimeupdate = null;
        }
    };

    function audioPreload(src, a){
        var aud = new Audio();
        aud.src = config.contentFolder + src;
        aud.oncanplay = function(i){
            audios[a].duration = aud.duration*1000;
            audioPreloadCounter++;
            sumAudioLength += aud.duration;
            if(audioPreloadCounter == audios.length){

                $playBtn.click(function(){

                    if(!isPlaying){
                        playThis();
                    } else {
                        stopThis();
                    }

                }).removeClass('loading');

            }
        };
    }

    function parseData(){

        var tempXml = xml;

        tempXml = tempXml.split('<sounds>')[1].split('</frames>')[0];
        tempXml = clear(tempXml, 'sound', 'b');
        tempXml = clear(tempXml, 'frame', 'i');

        var $manifest = $('<div/>').append(tempXml);
        $manifest.find('b').each(function(){
            audios.push({
                file: $(this).attr('File'),
                duration: 0
            });
        });
        $manifest.find('i').each(function(){
            var el = $(this);
            images.push({
                file: el.attr('File'),
                duration: el.attr('duration')
            })
        });

        $img.attr('src', config.contentFolder + images[0].file);

        audio.onended = function(){
            currentAudio++;

            if(currentAudio == audios.length) {
                stopThis();
                resetThis();
                audio.setAttribute('src', config.contentFolder + audios[currentAudio].file);
                isPlaying = false;
            }
            else if(isPlaying) {
                audio.setAttribute('src', config.contentFolder + audios[currentAudio].file);
                audio.play();
            }
        };

        audio.setAttribute('src', config.contentFolder + audios[currentAudio].file);


        $img.one('load', function() {

            var backgroundRatio = $img[0].naturalWidth / $img[0].naturalHeight;

            MMO.task.resizeHolder = MMO.helpers.throttle(function(){
                var resWidth,
                    resHeight,
                    areaHeight = window.innerHeight - $('.cp-button-container').outerHeight() - $header.outerHeight() - $text.outerHeight() - 20,
                    areaRatio = $area.width() / areaHeight;

                if(backgroundRatio > areaRatio){

                    resWidth = window.innerWidth;
                    resHeight =  'auto'

                } else {

                    resWidth = 'auto';
                    resHeight =  areaHeight;

                }

                $img.css({width: resWidth, height: resHeight});


            }, 200);

            $(window).trigger('resize');
        });

        $img.attr('src', config.contentFolder + images[currentImage].file);
    }

    function playThis(){

        $playBtn.addClass('playing');
        isPlaying = true;

        var timeout = images[currentImage].duration;

        if(currentTimeOffset){
            timeout = images[currentImage].duration - currentTimeOffset;

            currentTimeOffset = false;
        }

        nextImage = setTimeout(function(){
            currentImage++;

            if(currentImage == images.length) {
                clearTimeout(nextImage);
            }
            else if (isPlaying) {
                $img.attr('src', config.contentFolder + images[currentImage].file);
                playThis();
            }

        }, timeout);

        audio.play();

    }

    function stopThis(){
        $playBtn.removeClass('playing');
        isPlaying = false;

        currentTimeOffset = getCurrentImageTime();
        audio.pause();
        clearTimeout(nextImage);
    }

    function resetThis(){
        currentImage = 0;
        currentAudio = 0;
        currentTimeOffset = false;
    }

    function getCurrentImageTime(){
        var currAudioTime = audio.currentTime*1000;

        var prevAudioTime = function(){
            var t = 0;
            if(currentAudio > 0){
                var b = 0;
                while(b < currentAudio){t+=audios[b].duration;b++}
            }
            return t;
        };

        var globalAudioTime = currAudioTime + prevAudioTime();

        var prevImageTime = function(){
            var t = 0;
            if(currentImage > 0){
                var c = 0;
                while(c < currentImage){t+=images[c].duration;c++}
            }
            return t;
        };

        return (globalAudioTime - prevImageTime());
    }

});