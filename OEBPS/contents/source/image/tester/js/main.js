$(function(){

    var $paginator = $('<div/>').addClass('task__paginator'),
        $final = $('<div/>').addClass('task__final');

    var tasksArr = [], title = '', userAnswers = [];

    var current = 0,
        max = 0;

    var correct = 0, wrong = 0, missed = 0, maxPossible = 0,
        chosen = false;

    $check.addClass('test-mode');
    $restart.addClass('test-mode');
    $retry.hide();

    MMO.load.xml(function(){
        $sandbox.append(xml);

        var $tasks = $sandbox.find('task');

        $tasks.each(function(){
            var round = {};

            round.title = $(this).find('text').html();

            round.answers = [];

            $(this).find('answer').each(function(){
                round.answers.push({
                    txt: $(this).html(),
                    val: $(this).attr('value'),
                    img: $(this).attr('picture'),
                    imgTxt: $(this).attr('picture-text')
                });
            });

            tasksArr.push(round);

        });

        title = $sandbox.find('title').text();
        $header.append(title);

        initTask();

        $sandbox.find('style').appendTo('body');
    });

    function initTask(){

        var $test = $('<div/>').addClass('task__test');
        $area.append($test);

        tasksArr.forEach(function(item, i){

            var counter = 0,
                type = 'radio',
                $testItem = $('<div/>').addClass('task__test-item');

            var shuffled = MMO.helpers.shuffle(item.answers);

            shuffled.forEach(function(item, num){
                if(item.val == 1) counter++;

                var itemImage = (item.img) ? $('<img/>').addClass('test-item__answer-img').attr('src', config.contentFolder + item.img) : '';
                if(item.imgTxt) itemImage.attr('data-txt', item.imgTxt);

                $(itemImage).click(function(e){
                    e.preventDefault();
                    MMO.plugins.showFullscreenImage($(this).attr('src'), $(this).attr('data-txt'));
                });

                $testItem.append(

                    $('<label/>')
                        .addClass('test-item__answer')
                        .append(
                        $('<input/>')
                            .addClass('test-item__answer-input')
                            .attr('data-val', item.val)
                            .attr('name', 'test-answer-' + i)
                            .attr('id', 'answer-' + i + '-' + num),
                        $('<span/>')
                            .attr('for', 'answer-' + i + '-' + num)
                            .addClass('test-item__answer-text')
                            .append(
                            itemImage,
                            '<span class="test-item__answer-content">' + item.txt + '</span>'
                        )
                    )
                );
            });

            if(counter > 1) type = 'checkbox';

            $testItem.find('.test-item__answer').children('input')
                .attr('type', type)
                .click(function(){
                    chosen = true;
                    $check.addClass('active');
                });

            $test.append($testItem);

        });

        max = tasksArr.length;

        $area.append($final);

        $area.append($paginator);
        $check.click(toggleTaskItem).show();
        $restart.click(restartTest).hide();

        for(var a = 0; a < tasksArr.length; a++){
            $paginator.append(
                $('<div/>')
                    .addClass('task__paginator-item')
                    .append(
                    $('<div/>')
                        .addClass('item-ind-1'),
                    $('<div/>')
                        .addClass('item-ind-2'),
                    $('<div/>')
                        .addClass('item-text')
                        .text(a + 1)
                )
            );
        }

        $('.task__test-item').hide().eq(current).show();
        $('.task__paginator-item').eq(current).addClass('active');
        $text.html('').append(tasksArr[current].title);
    }

    function toggleTaskItem(){

        if(!chosen) return false;
        $check.removeClass('active');
        chosen = false;

        var localCorrect = 0,
            localWrong = 0,
            localMissed = 0;

        var $question = $('<div/>').addClass('user-answer').text($('.task__text').text()).css({color: '#000', fontWeight: 700});
        userAnswers.push($question);

        $('.task__test-item').eq(current).find('.test-item__answer input').each(function(){

            var checked = $(this).prop('checked'),
                val = !!+($(this).attr('data-val')),
                multi = $(this).attr('type') == 'checkbox';

            var $answer = $('<div/>').addClass('user-answer').text($(this).next().text());

            if(checked){

                if(val){

                    localCorrect++;
                    maxPossible++;
                    $answer.css('color', '#72BD08').append(' (Правильно)');

                }
                else {
                    localWrong++;
                    $answer.css('color', 'red').append(' (Неправильно)');
                }

                userAnswers.push($answer);

            }
            else {
                if(val){

                    //$answer.css('color', '#ebebeb').append(' (Пропущено)');
                    //userAnswers.push($answer);

                    if(multi)localMissed++;
                    maxPossible++;
                }
            }

        });

        if(localWrong || localMissed){

            if(!localCorrect && !localWrong){

                $('.task__paginator-item').eq(current).find('.item-ind-1').css({
                    height: '100%',
                    background: '#ebebeb'
                });

            }

            else if (!localCorrect){

                $('.task__paginator-item').eq(current).find('.item-ind-1').css({
                    height: '100%',
                    background: 'red'
                });

            }

            else if(localCorrect){

                var errors = localWrong + localMissed;

                var part = 100 / (localCorrect + errors);

                $('.task__paginator-item').eq(current).find('.item-ind-1').css({
                    height: localCorrect*part + '%',
                    background: '#97c25a'
                });

                $('.task__paginator-item').eq(current).find('.item-ind-2').css({
                    height: errors*part + '%',
                    top: localCorrect*part + '%',
                    background: 'red'
                });

            }


        } else {

            $('.task__paginator-item').eq(current).find('.item-ind-1').css({
                height: '100%',
                background: '#97c25a'
            });

        }

        correct+=localCorrect;
        wrong+=localWrong;
        missed+=localMissed;

        current++;

        if(current != max){

            $('.task__test-item').hide().eq(current).show();
            $('.task__paginator-item').removeClass('active').eq(current).addClass('active');

            $text.html('').append(tasksArr[current].title);

        } else {

            $('.task__test-item').hide();
            $('.task__paginator-item').removeClass('active');
            $text.html('').append('Тест завершён');

            $final.html('').append(
                $('<span/>').text('Правильных ответов: ' + correct),
                $('<span/>').text('Неправильных ответов: ' + wrong)
                //$('<span/>').text('Пропущено: ' + missed), '<br/>'
            );

            //var grade = (correct/(correct + wrong + missed)).toFixed(3) + '';

            var all = correct + wrong + missed,
                grade = (correct/(all/100)).toFixed(2) + '';

            //if(grade.indexOf('.') != -1)grade = grade.replace('.', ','); тут раскомментировать

            if(missed || wrong) MMO.task.result.show(false, grade);
            else MMO.task.result.show(true, grade);

            userAnswers.forEach(function(item){
                $final.append(item);
            });

            $final.show();
            $restart.show();
            $check.hide();
        }
    }

    function restartTest(){
        $restart.hide();
        $check.show();

        current = 0;
        userAnswers = [];
        correct = 0;
        missed = 0;
        wrong = 0;

        $final.hide();

        $('.task__test-item').hide().eq(current).show();
        $('.task__paginator-item').eq(current).addClass('active');

        $('.task__paginator-item .item-ind-1, .task__paginator-item .item-ind-2').removeAttr('style');
        $('.test-item__answer input').each(function(){
            if($(this).prop('checked'))$(this).prop('checked', false);
        });

        $text.html('').append(tasksArr[current].title);

        chosen = false;
        $check.removeClass('active');
    }

});