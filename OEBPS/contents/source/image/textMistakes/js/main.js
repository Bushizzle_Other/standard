$(function(){

    var $container = $('#media-object');

    var $inputs = null,
        $activeMark = null,
        $marksPopup = $('<div class="TM__popup"><div class="TM__popup-close"></div><span class="TM__popup-h">Выбери знак препинания:</span></div>').appendTo($container),
        blocked = false;

    var $tmOverlay = $('<div class="TM__overlay"></div>')
        .click(function(){
            if(!blocked){
                $(this).fadeOut();
                $marksPopup.fadeOut();
            }
        })
        .appendTo($container);

    var $cashTask = null;

    MMO.load.xml(
        function(){
            $sandbox.append(xml);

            var hText = $sandbox.find('title').text(),
                tText = $sandbox.find('question').html();

            hText.length ? $header.append(hText) : $header.hide();
            tText.length ? $text.append(tText) : $text.hide();

            var $taskText = $sandbox.find('task');

            var marks = $taskText.attr('options') ? $taskText.attr('options').split('$') :  [' ', ', ', '. ', ': ', '; ', ' - ', '! ', '? ', ' «', '» ', ' „', '“ '];

            marks.forEach(function(item){
                $marksPopup.append($('<div class="TM__popup-item"></div>').attr('data-val', item).text(item));
            });

            $taskText.find('section').each(function(){
                var sectionRole = $(this).attr('data-role');

                if(sectionRole == 'content'){

                    $area.append('<div class="W__section W__section-content">' + $(this).html() + '</div>');

                }
                else if(sectionRole == 'text'){

                    $(this).find('span').each(function(){

                        var type = $(this).attr('data-t');

                        if(type == 't'){
                            $(this).attr('contenteditable', true).addClass('checkable');
                        }
                        else if(type == 'm'){
                            $(this).addClass('mark-btn');
                        }

                        if($(this).text() == ' ') $(this).text('|').addClass('space');
                        if($(this).attr('data-c') == ' ')$(this).attr('data-c', '|');

                    });

                    $area.append($(this));

                    $cashTask = $area.children().clone();

                }
            });

            $inputs = $('.mark-btn, .checkable');

            $sandbox.find('style').appendTo('body');
        }
    );

    $container
        .on('click', '.mark-btn', function(){
            $activeMark = $(this).addClass('edited');
            $marksPopup.add($tmOverlay).fadeIn();
        })
        .on('click', '.TM__popup-item', function(){
            var val = $(this).attr('data-val');
            if(val == ' '){
                val = '|';
                $activeMark.addClass('space');
            } else {
                $activeMark.removeClass('space');
            }
            $activeMark.addClass('edited').html(val);
            $marksPopup.add($tmOverlay).fadeOut();
        })
        .on('click', '.TM__popup', function(e){
            e.stopPropagation();
        })
        .on('click', '.TM__popup-close', function(){
            $marksPopup.add($tmOverlay).fadeOut();
        });

    $check.click(function(){

        blocked = true;

        var mistakes = 0;

        $inputs.each(function(){
            var val = $(this).html(),
                correct = $(this).attr('data-c').split('$');

            if(correct.indexOf(val) == -1) {
                mistakes++;

                if($(this).hasClass('edited')) $(this).addClass('TM__mistake');
            }

        });

        MMO.task.result.show(!mistakes);
        $tmOverlay.fadeIn();
        $marksPopup.hide();
    });

    $restart.add($retry).click(function(){
        blocked = false;

        $area.html('').append($cashTask.clone());

        $tmOverlay.fadeOut();
    });

});