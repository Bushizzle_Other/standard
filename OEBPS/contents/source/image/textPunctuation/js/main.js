$(function(){

    var marks = [',', '.', '-', ':', ';'],
        $inputs = null;

    MMO.load.xml(
        function(){
            $sandbox.append(xml);

            var hText = $sandbox.find('title').text(),
                tText = $sandbox.find('question').html();

            hText.length ? $header.append(hText) : $header.hide();
            tText.length ? $text.append(tText) : $text.hide();

            var $taskText = $sandbox.find('task');

            $taskText.find('section').each(function(){
                var sectionRole = $(this).attr('data-role');

                if(sectionRole == 'content'){

                    $area.append('<div class="W__section W__section-content">' + $(this).html() + '</div>');

                }
                else if(sectionRole == 'text'){

                    var text = '',
                        textArr = $(this).text().split(' ');

                    textArr.forEach(function(word){

                        var mark = null,
                            field = '<span contenteditable="true" class="TP__input"></span>';

                        marks.forEach(function(item){

                            if(word.indexOf(item) != -1){
                                mark = item;
                                field = '<span contenteditable="true" class="TP__input" data-correct="' + mark + '"></span>';
                            }
                        });

                        if(mark) word = word.replace(mark, field);
                        else word += field;

                        text+=word;

                    });

                    $area.append(text);

                }
                else if(sectionRole == 'text-static'){

                    $area.append($(this).html());

                }
            });

            $inputs = $('.TP__input');

            $sandbox.find('style').appendTo('body');
        }
    );

    $area.on('keyup', '.TP__input', function(){
        $(this).addClass('edited');

        var val = $(this).text();
        if(val.length > 1) $(this).text(val.substr(0,1));

        if(val == '-')$(this).css({
            textAlign: 'center',
            padding: '0 6px'
        });
        else $(this).css({
            textAlign: 'left',
            padding: '0'
        });
    });

    $check.click(function(){

        var mistakes = 0;

        $inputs.each(function(){
            var val = $(this).text(),
                correct = $(this).attr('data-correct');

            if(!correct && val || correct && val != correct) {
                if($(this).hasClass('edited'))$(this).addClass('TP__mistake');
                mistakes++;
            }

        });

        MMO.task.result.show(!mistakes);
        $area.addClass('TP__blocked');
    });

    $restart.add($retry).click(function(){
        $inputs
            .removeClass('TP__mistake')
            .text('');

        $area.removeClass('TP__blocked');
    });

});