$(function(){

    var vars = [], wrongVars = [], image;

    var $captionsArea = $('<div/>').addClass('C__area'),
        $captionsBG = $('<img/>').addClass('C__background'),
        $vars = $('<div/>').addClass('C__vars');

    MMO.load.libs([['$.ui','core/scripts/libs/jquery-ui-1.9.2.custom.min.js']]);

    MMO.load.xml(function(){

        var markup = xml;
        while(markup.indexOf('area') != -1){markup = markup.replace('area', 'box')}
        $sandbox.append(markup);

        $header.append($sandbox.find('title').html());
        $text.append($sandbox.find('question').html());

        //

        $sandbox.find('box').each(function(){
            var box = {
                x: $(this).attr('x'),
                y: $(this).attr('y'),
                width: $(this).attr('width'),
                height: $(this).attr('height'),
                correct: $(this).text()
            };

            vars.push(box);
        });

        $sandbox.find('wrongWords').text().split('#').forEach(function(item){

            if(item != '')wrongVars.push({correct: item});

        });

        image = config.contentFolder + $sandbox.find('background').text();

        initTask();

        $sandbox.find('style').appendTo('body');

    });

    function initTask(){

        $captionsBG.attr('src', image);

        $area.append($captionsArea.append($captionsBG));

        $captionsBG.load(function(){
            $captionsArea.css({
                width: $captionsBG.width() + 160,
                height: $captionsBG.height()
            });

            $captionsArea.append($vars);

            initDrops();
            prepareDrags();
        });

    }

    function initDrops(){
        vars.forEach(function(item){

            $captionsArea.append(
                $('<div/>')
                    .addClass('C__drop')
                    .css({
                        left: item.x + 'px',
                        top: item.y + 'px',
                        width: item.width,
                        height: item.height
                    })
                    .attr('data-val', item.correct)
            );


        });

        $('.C__drop').droppable({
            drop: function(event, ui){

                if($(this).children().length == 0){

                    var $dragging = $(ui.draggable).clone(),
                        id = $dragging.attr('id'),
                        $ancestor = $('#' + id);

                    $ancestor.remove();

                    $dragging
                        .draggable({
                            cursorAt: {
                                left: 75,
                                top: 10
                            },
                            helper: function() {
                                return $(this).clone().width(150).css({zIndex: 100, opacity:.8});
                            },
                            start: function(event, ui){

                            },
                            stop: function(event, ui) {

                            }
                        })

                    $(this)
                        .removeClass('hover')
                        .append($dragging);

                }
            },
            over: function(){
                $(this).addClass('hover');
            },
            out: function() {
                $(this).removeClass('hover');
            }
        });

        if(wrongVars.length)vars = vars.concat(wrongVars);
    }

    function prepareDrags(){

        $('.C__drop').removeClass('error').html('');
        $vars.html('');

        vars = MMO.helpers.shuffle(vars);

        vars.forEach(function(item){

            if(item.hasOwnProperty()){

            } else {

            }

            $vars.append(
                $('<div/>')
                    .addClass('C__vars-item')
                    .attr('id', 'drag-' + Math.round(Math.random()*100000))
                    .attr('data-val', item.correct)
                    .append(item.correct)
            );

        });

        $('.C__vars-item').draggable({
            cursorAt: {
                left: 75,
                top: 10
            },
            helper: function() {
                return $(this).clone().width(150).css({zIndex: 100, opacity:.8});
            }
        });

    }

    function checkTask(){

        var errors = false;

        $('.C__drop').each(function(){

            var correct = $(this).attr('data-val'),
                val = $(this).find('.C__vars-item').attr('data-val');

            if(val != correct){
                errors = true;
                $(this).addClass('error');
            }

        });

        if(errors){
            MMO.task.result.show(false);
        }
        else {
            MMO.task.result.show(true);

        }

    }


    $check.click(checkTask);
    $restart.add($retry).click(prepareDrags);

});