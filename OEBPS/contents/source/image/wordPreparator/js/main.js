$(function(){

    //rangyReady var ?

    var $task = $('<div class="W__task"></div>').appendTo($area),
        $taskPrototype = $('<div></div>');

    var highlighter,
        allHighLightClasses = {
            'prefix': 'Приставка',
            'root': 'Корень',
            'divider': 'Соединительная буква',
            'suffix': 'Суффикс',
            'base' : 'Основа',
            'ending' : 'Окончание',
            'zero-ending' : 'Нулевое окончание',
            'accent': 'Ударение',

            'color-R':'Красный',
            'color-G':'Зелёный',
            'color-B':'Синий',

            'subject' : 'Подлежащее',
            'predicate' : 'Сказуемое',
            'definition' : 'Определение',
            'addition' : 'Дополнение',
            'circumstance' : 'Обстоятельство',

            'main': 'Главная часть',
            'subordinate': 'Придаточная часть',
            'comparative' : 'Сравнительный оборот'
        },
        highLightClasses = {},
        currentHighlightClass = null, //option.className
        highlighted = {};

    var sentenceParts = ['subject', 'predicate', 'definition', 'appendix', 'addition', 'circumstance'];
    var sentenceGroups = ['main', 'subordinate', 'comparative'];

    var controlSum = 0;

    MMO.load.libs([['rangy','core/scripts/libs/rangy-custom.js']], false, MMO.load.xml(
        function(){
            $sandbox.append(xml);

            var hText = $sandbox.find('title').text(),
                tText = $sandbox.find('question').html();

            hText.length ? $header.append(hText) : $header.hide();
            tText.length ? $text.append(tText) : $text.hide();

            var $taskText = $sandbox.find('task');

            var taskClasses = $taskText.attr('options');

            if(taskClasses) {
                taskClasses = taskClasses.split(',');
                taskClasses.forEach(function(item){
                    if(item.indexOf(':') != -1) {
                        var newName = item.split(':');
                        highLightClasses[newName[0]] = newName[1];
                    } else {
                        highLightClasses[item] = allHighLightClasses[item];
                    }
                });
                currentHighlightClass = taskClasses[0];
            } else {
                highLightClasses = allHighLightClasses;
                currentHighlightClass = 'prefix';
            }

            $taskText.find('section').each(function(){

                var sectionRole = $(this).attr('data-role');

                if(sectionRole == 'content'){

                    $taskPrototype.append('<div class="W__section W__section-content">' + $(this).html() + '</div>');

                }
                else if(sectionRole == 'sentence'){

                    var $section = $('<div class="W__section W__section-text"></div>');

                    $(this).find('text').each(function(){

                        var textRole = $(this).attr('data-role') ? $(this).attr('data-role').split(' ') : false,
                            zeroEnding = textRole ? textRole.includesOneOf(['zero-ending']) : false,
                            sentenceObj = textRole ? textRole.includesOneOf(sentenceParts) : false,
                            wordId = MMO.helpers.randomId(),
                            additionalDataType = [];

                        var groupCounter = 0,
                            inBase = false,
                            inGroup = false;

                        [sentenceObj, zeroEnding].forEach(function(item){
                            if(item) {
                                additionalDataType.push(textRole[item]);
                                groupCounter++;
                            }
                        });

                        $(this).find('span').each(function(){

                            var dataType = $(this).attr('data-type') ? $(this).attr('data-type').split(' ') : [],
                                text = $(this).text(),
                                groupId = MMO.helpers.randomId();

                            var style = $(this).attr('style'),
                                styleString = '';

                            if(style){
                                if(style.indexOf('bold') != -1)styleString+='font-weight:bold;';
                                if(style.indexOf('italic') != -1)styleString+='font-style:italic;';
                                if(style.indexOf('color') != -1)styleString+= 'color:' + style.split('color:')[1] + ';';
                            }

                            if(dataType.length) {

                                var dataTypeArr = dataType.slice();

                                // base detector

                                if(dataTypeArr.includes('base')){
                                    inBase = true;
                                    dataTypeArr.splice(dataTypeArr.indexOf('base'), 1);
                                } else {
                                    if(inBase)groupCounter++;
                                    inBase = false;
                                }

                                // sentence group detector

                                var wordInGroup = dataTypeArr.includesOneOf(sentenceGroups);

                                if(wordInGroup){
                                    inGroup = true;
                                    dataTypeArr.splice(dataTypeArr.indexOf(dataTypeArr[wordInGroup]), 1);
                                } else {
                                    if(inGroup)groupCounter++;
                                    inGroup = false;
                                }

                                groupCounter += dataTypeArr.length;
                            } else {
                                if(inBase)groupCounter++;
                                inBase = false;

                                if(inGroup)groupCounter++;
                                inGroup = false;
                            }

                            if(additionalDataType.length) dataType = dataType.concat(additionalDataType);

                            text.split('').forEach(function(letter, num){
                                var offset = (!num && style && style.indexOf('offset') != -1) ? 'margin-left:30px;' : '';
                                $section.append('<span ' + (style ? ('style="' + styleString + offset + '"') : '') + ' class="W__letter' + (dataType.length ? ' W__letter-useful' : ' W__letter-useless') + '" data-word-id="' + wordId + '" data-group-id="' + groupId + '" data-type="' + dataType.join(' ') + '">' + letter + '</span>');
                            });

                        });

                        if(inBase)groupCounter++;

                        controlSum += groupCounter;

                    });

                    $taskPrototype.append($section);

                }
            });


            $task.append($taskPrototype.clone().children());

            initTask();

            $sandbox.find('style').appendTo('body');
        }
    ));

    function initTask(){

        // controls & rangy init

        var $wChoose = $('<select class="W__select"></select>')
            .change(function(){
                currentHighlightClass = $(this).val();
            });

        rangy.init();

        highlighter = rangy.createHighlighter();

        for(var className in highLightClasses){

            highlighter.addClassApplier(rangy.createClassApplier(className, {
                ignoreWhiteSpace: true,
                useExistingElements: false,
                onElementCreate: function(el){
                }
            }));

            $wChoose.append('<option value="' + className + '">' + highLightClasses[className] + '</option>');

        }

        var $wControls = $('<div class="W__controls"></div>').appendTo($area),
            $wSelect = $('<button type="button" title="Выделить" class="W__controls-btn W__select">Выделить</button>').appendTo($wControls);

        $wControls.append($wChoose);

        $wSelect.click(function(){
            highlightSelectedText();
        });


        // highlight process

        function clearSelection() {
            if ( document.selection ) {
                document.selection.empty();
            } else if ( window.getSelection ) {
                window.getSelection().removeAllRanges();
            }
        }

        function highlightSelectedText() {

            highlighter.highlightSelection(currentHighlightClass);

            // create highlighted obj

            var $highlightedGroup =  $('.' + currentHighlightClass).parent(),
                id = MMO.helpers.randomId();

            if(!$highlightedGroup.length) return 0;

            var newHighlighted = {
                $start: $highlightedGroup.eq(0),
                $end: $highlightedGroup.eq(-1),
                id: id,
                type: currentHighlightClass,
                correct: false
            };

            highlighted[id] = newHighlighted;

            highlighter.removeAllHighlights();
            clearSelection();

            //TODO возможно ли это без highlighter-а и classapplier-a?

            // check highlight

            if(newHighlighted.type == 'zero-ending' || sentenceParts.includes(newHighlighted.type)){

                if(newHighlighted.$end.attr('data-type').indexOf(newHighlighted.type) != -1) newHighlighted.correct = true;

            }
            else if(newHighlighted.type == 'base' || sentenceGroups.includes(newHighlighted.type)){

                var noOtherTypeInsideGroup = $highlightedGroup.filter(function(){
                    return $(this).attr('data-type').indexOf(newHighlighted.type) == -1;
                });

                if(!noOtherTypeInsideGroup.length){
                    // all letters has the same data type

                    var prevLetterOtherType = !newHighlighted.$start.prev().attr('data-type') || newHighlighted.$start.prev().attr('data-type').indexOf(newHighlighted.type) == -1,
                        nextLetterOtherType = !newHighlighted.$end.next().attr('data-type') || newHighlighted.$end.next().attr('data-type').indexOf(newHighlighted.type) == -1;

                    if(prevLetterOtherType && nextLetterOtherType){
                        // $start && $end are current type ends

                        newHighlighted.correct = true;
                    }

                }

            }
            else {

                var firstGroup = newHighlighted.$start.attr('data-group-id'),
                    prevGroup = (newHighlighted.$start.index() == 0) ? 'correct' : newHighlighted.$start.prev().attr('data-group-id'),
                    endGroup = newHighlighted.$end.attr('data-group-id'),
                    nextGroup = (newHighlighted.$end.is(':last-child')) ? 'correct' : newHighlighted.$end.next().attr('data-group-id'),
                    singleGroup = $("[data-group-id='" + firstGroup + "']").length == $highlightedGroup.length;

                if(firstGroup != prevGroup && endGroup != nextGroup && singleGroup){

                    //console.log('group is correct');

                    var correctType = newHighlighted.$start.attr('data-type');

                    if(correctType.indexOf(newHighlighted.type) != -1){

                        //console.log('data-type is correct');

                        newHighlighted.correct = true;
                    }

                }

            }

            // draw highlight

            if(newHighlighted.type == 'zero-ending' || sentenceParts.includes(newHighlighted.type)){
                var wordId = newHighlighted.$end.attr('data-word-id'),
                    $wordLetters = $("[data-word-id='" + wordId + "']");
            }

            if(newHighlighted.type == 'zero-ending'){

                if($wordLetters.eq(-1).next().hasClass('W__zero-ending')) return 0;

                newHighlighted.$el = $('<div title="Удалить" class="W__zero-ending"></div>')
                    .insertAfter($wordLetters.eq(-1))
                    .click(function(){
                        delete highlighted[id];
                        $(this).remove();
                    });
            }
            else if(sentenceGroups.includes(newHighlighted.type)){

                var $sentenseGroupStart = $('<div class="W__sentence-group W__' + newHighlighted.type + '-start"></div>').insertBefore(newHighlighted.$start),
                    $sentenseGroupEnd = $('<div class="W__sentence-group W__' + newHighlighted.type + '-end"></div>').insertAfter(newHighlighted.$end);

                newHighlighted.$el = $sentenseGroupStart.add($sentenseGroupEnd);

                $sentenseGroupStart.add($sentenseGroupEnd).click(function(){
                    $sentenseGroupStart.add($sentenseGroupEnd).remove();
                    delete highlighted[id];
                });

            }
            else {

                var highlightWidth,
                    $hightlightContainer;

                if(sentenceParts.includes(newHighlighted.type)) {
                    //highlightWidth = $wordLetters.eq(-1).offset().left - $wordLetters.eq(0).offset().left + $wordLetters.eq(-1).width();
                    //$hightlightContainer = $wordLetters.eq(0);

                    if(newHighlighted.type == 'definition' || newHighlighted.type == 'circumstance'){

                        highlightWidth = newHighlighted.$end.offset().left - newHighlighted.$start.offset().left + newHighlighted.$end.width();

                        newHighlighted.$el = $('<div title="Удалить" class="W__highlight W__' + currentHighlightClass + '"></div>')
                            .width(highlightWidth)
                            .click(function(){
                                delete highlighted[id];
                                $(this).remove();

                            });

                        $highlightedGroup.eq(0).append(newHighlighted.$el);

                    } else {

                        $highlightedGroup.each(function(){
                            var $letter = $(this);

                            $letter.append(
                                $('<div title="Удалить" class="W__highlight W__' + currentHighlightClass + '"></div>')
                                    .width($letter.width())
                                    .click(function(){
                                        delete highlighted[id];
                                        $highlightedGroup.each(function(){
                                            $(this).find('.W__' + currentHighlightClass).remove();
                                        });
                                    })
                            );

                            newHighlighted.$el = $highlightedGroup.find('.W__' + currentHighlightClass);
                        });

                    }
                }
                else {
                    highlightWidth = newHighlighted.$end.offset().left - newHighlighted.$start.offset().left + newHighlighted.$end.width();
                    $hightlightContainer = newHighlighted.$start;

                    newHighlighted.$el = $('<div title="Удалить" class="W__highlight W__' + currentHighlightClass + '"></div>')
                        .width(highlightWidth)
                        .appendTo($hightlightContainer)
                        .click(function(){
                            delete highlighted[id];
                            $(this).remove();
                        });
                }

                if(newHighlighted.type == 'divider') {

                    var letterWidth = newHighlighted.$start.width(),
                        $dividerBefore = $('<div class="W__space"></div>').width(letterWidth *.75).insertBefore(newHighlighted.$start),
                        $dividerAfter = $('<div class="W__space"></div>').width(letterWidth *.75).insertAfter(newHighlighted.$end);

                    newHighlighted.$el
                        .css({
                            width: (highlightWidth + letterWidth) + 'px',
                            left: -letterWidth/2 + 'px'
                        })
                        .click(function(){
                        $dividerBefore.add($dividerAfter).remove();
                    });

                }
            }

            console.log(highlighted);
        }

    }

    function checkSelection(){

        var mistakes = 0,
            count = 0;

        for(var key in highlighted){
            if(!highlighted[key].correct) {
                highlighted[key].$el.addClass('W__highlight-mistake');
                mistakes++;
            } else {
                highlighted[key].$el.addClass('W__highlight-correct');
            }
            count++;
        }

        if(count && count == controlSum && !mistakes)MMO.task.result.show(true);
        else MMO.task.result.show(false);

    }

    $check.click(function(){
        checkSelection();
        $area.addClass('W__blocked');
    });

    $restart.add($retry).click(function(){
        $task
            .html('')
            .append($taskPrototype.clone().children());

        $area.removeClass('W__blocked');
        highlighted = {};
    });

});