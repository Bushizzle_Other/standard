var xml='<?xml version="1.0" encoding="utf-8"?>' +
    '<component>' +
    '<title>Разбор слова</title>' +
    '<question>Разбери слово</question>	' +
    '<task>' +

    '<section data-role="content">' +
        '<img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH1ggDCwMADQ4NnwAAAFVJREFUGJWNkMEJADEIBEcbSDkXUnfSgnBVeZ8LSAjiwjyEQXSFEIcHGP9oAi+H0Bymgx9MhxbFdZE2a0s9kTZdw01ZhhYkABSwgmf1Z6r1SNyfFf4BZ+ZUExcNUQUAAAAASUVORK5CYII="/>' +
        '<h3>Это просто заголовок, его никак не выделить. Над ним - маленькая картинка.</h3>' +
        '<br />' +
    '</section>' +

    '<section data-role="sentence">' +

        '<text data-role="word">' +
            '<span>В лесу росли </span>' +
            '<span data-type="main">под</span>' +
            '<span data-type="main">осин</span>' +
            '<span data-type="main">ов</span>' +
            '<span data-type="main">ик</span>' +
            '<span data-type="main">и</span>' +
            '<span> и </span>' +
            '<span data-type="main">бананы</span>' +
            '<span>.</span>' +
        '</text>' +

    '</section>' +

    '</task>' +

    '</component>';