var xml='<?xml version="1.0" encoding="utf-8"?>' +
    '<component>' +
    '<title> </title>' +
    '<question>Проведите морфемный разбор предложенных слов.</question>	' +
    '<task options="prefix,root,divider,suffix,base,ending,zero-ending">' +
    '<task>' +

    '<section data-role="sentence">' +

    '<text data-role="word zero-ending">' +
    '<span style="offset" data-type="prefix base">Раз</span>' +
    '<span data-type="root base">говор</span>' +
    '<span data-type="suffix base">чив</span>' +
    '<span data-type="suffix base">ость</span>' +
    '</text>' +

    '<text>' +
    '<span>, </span>' +
    '</text>' +

    '<text data-role="word">' +
    '<span style="italic color:red" data-type="root base">солов</span>' +
    '<span style="bold" data-type="suffix base">ушк</span>' +
    '<span data-type="ending">а</span>' +
    '</text>' +

    '<text>' +
    '<span>.</span>' +
    '</text>' +

    '</section>' +

    '</task>' +
    '<style>.W__controls:before{content: "Выбери часть слова:"}</style>' +
    '</component>';