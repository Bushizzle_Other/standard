var xml='<?xml version="1.0" encoding="utf-8"?>' +
    '<component>' +
    '<title>Разбор <i>слова</i></title>' +
    '<question>Разбери <i>слово</i></question>	' +
    '<task options="prefix,root,divider,suffix,base,ending,zero-ending:Корненоль">' +

    '<section data-role="content">' +
        '<img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH1ggDCwMADQ4NnwAAAFVJREFUGJWNkMEJADEIBEcbSDkXUnfSgnBVeZ8LSAjiwjyEQXSFEIcHGP9oAi+H0Bymgx9MhxbFdZE2a0s9kTZdw01ZhhYkABSwgmf1Z6r1SNyfFf4BZ+ZUExcNUQUAAAAASUVORK5CYII="/>' +
        '<h3>Это просто заголовок, <b>его</b> никак не выделить. Над ним - маленькая картинка.</h3>' +
        '<br />' +
    '</section>' +

    '<section data-role="sentence">' +

        '<text data-role="word main">' +
            '<span style="offset">(</span>' +
            '<span style="italic">В лесу росли</span>' +
            '<span>) </span>' +
        '</text>' +

        '<text data-role="word subject main">' +
            '<span data-type="prefix base">под</span>' +
            '<span data-type="root base">осин</span>' +
            '<span data-type="suffix base">ов</span>' +
            '<span data-type="suffix base">ик</span>' +
            '<span data-type="ending">и</span>' +
        '</text>' +

        '<text data-role="word main">' +
            '<span>, </span>' +
        '</text>' +

        '<text data-role="word main">' +
            '<span data-type="prefix base">под</span>' +
            '<span data-type="root base">берез</span>' +
            '<span data-type="suffix base">ов</span>' +
            '<span data-type="suffix base">ик</span>' +
            '<span data-type="ending">и</span>' +
        '</text>' +

        '<text>' +
            '<span> и один яркий, но бесполезный </span>' +
        '</text>' +

        '<text data-role="word zero-ending">' +
            '<span data-type="root base">мух</span>' +
            '<span data-type="divider">о</span>' +
            '<span data-type="root base">мор</span>' +
        '</text>' +

        '<text>' +
            '<span>.</span>' +
        '</text>' +

    '</section>' +

    '</task>' +

    '</component>';