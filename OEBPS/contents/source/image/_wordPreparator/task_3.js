var xml='<?xml version="1.0" encoding="utf-8"?>' +
    '<component>' +
    '<title> </title>' +
    '<question>Разбери по членам предложения.</question>	' +
    '<task options="subject,predicate,definition,addition,circumstance">' +
    '<task>' +

    '<section data-role="sentence">' +

    '<text data-role="addition">' +
    '<span>Нам</span>' +
    '</text>' +

    '<text>' +
    '<span> </span>' +
    '</text>' +

    '<text data-role="circumstance">' +
    '<span>сегодня</span>' +
    '</text>' +

    '<text>' +
    '<span> </span>' +
    '</text>' +

    '<text data-role="predicate">' +
    '<span>не задали</span>' +
    '</text>' +

    '<text>' +
    '<span> </span>' +
    '</text>' +

    '<text data-role="addition">' +
    '<span>домашнего</span>' +
    '</text>' +

    '<text>' +
    '<span> </span>' +
    '</text>' +

    '<text data-role="addition">' +
    '<span>задания</span>' +
    '</text>' +

    '<text>' +
    '<span>, </span>' +
    '</text>' +

    '<text>' +
    '<span>потому что</span>' +
    '</text>' +

    '<text>' +
    '<span> </span>' +
    '</text>' +

    '<text data-role="subject">' +
    '<span>мы с классом</span>' +
    '</text>' +

    '<text>' +
    '<span> </span>' +
    '</text>' +

    '<text data-role="predicate">' +
    '<span>поехали</span>' +
    '</text>' +

    '<text>' +
    '<span> </span>' +
    '</text>' +
        
    '<text data-role="circumstance">' +
    '<span>в театр</span>' +
    '</text>' +

    '<text>' +
    '<span>.</span>' +
    '</text>' +

    '</section>' +

    '</task>' +
    '<style>.W__controls:before{content: "Выбери часть предложения:"}</style>' +
    '</component>';