$(function(){

    $check.add($restart).add($retry).add($text).add($header).remove();

    var src;

    if(config.content.indexOf('.mp4') != -1){

        src = config.contentFolder + config.content.split('/')[1];
        videoPreload(src);
        MMO.task.afterInit();

    } else {

        MMO.load.xml(function(){
            $sandbox.append(xml);
            src = config.contentFolder + $sandbox.find('video').text() + '.mp4';
            videoPreload(src);
        });

    }

    function videoPreload(src){
        var video = document.createElement('video'),
            percent = ['0.05','0.10','0.15','0.20','0.25','0.30','0.35','0.40', '0.45','0.50','0.55','0.60','0.65','0.70','0.75','0.80','0.85','0.90','0.95'],
            gradeReady = false;

        video.controls = true;
        $area.append(video);

        video.ontimeupdate = function(){

            if(!gradeReady){
                var currentProgress = (video.currentTime/video.duration).toFixed(2);

                if(percent.includes(currentProgress)) {
                    percent.splice(percent.indexOf(currentProgress), 1);
                }

                if(!percent.length) {
                    MMO.task.result.show(true, 100, true);
                    gradeReady = true;
                }
            } else {
                video.ontimeupdate = null;
            }
        };

        video.oncanplay = function(){

            $(video).css({
                display: 'block'
            });

            var ratio = $(video).width() / $(video).height();

            $(window).resize(function (){

                if(window.innerWidth > window.innerHeight*ratio)$(video).css({height: '95%', width: 'auto', margin: '0 auto'});
                else $(video).css({width: '90%', height: 'auto', margin: '0 5%'});

            });

            video.play();

            $(window).trigger('resize');
        };
        video.src = src;
    }

});