var xml='<?xml version="1.0" encoding="utf-8"?>' +
    '<component>' +
    '<title><i>Ошибки</i></title>' +
    '<question>Тыща <b>их.</b> Исправь</question>	' +
    '<task options=".$ $! $? ">' +

    '<section data-role="content">' +
        '<h3>Это просто заголовок</h3>' +
        '<br />' +
    '</section>' +

    '<section data-role="text">' +
        '<span style="color:red;font-weight:bold;" data-t="t" data-c="В">В</span>' +
        '<span data-t="m" data-c=" "> </span>' +
        '<span data-t="t" data-c="лесу">лису</span>' +
        '<span data-t="m" data-c=" ">: </span>' +
        '<span data-t="t" data-c="родилась">радилас</span>' +
        '<span data-t="m" data-c=" ">, </span>' +
        '<span data-t="t" data-c="ёлочка">йолачка</span>' +
        '<span data-t="m" data-c=". ">, </span>' +
    '</section>' +

    '</task>' +

    '</component>';