var xml='<?xml version="1.0" encoding="utf-8"?>' +
    '<component>' +
    '<title> </title>' +
    '<question>Расставьте пропущенные знаки препинания в предложениях.</question>	' +
    '<task options=" $. $, $! $? $ – $: ">' +

    '<section data-role="text">' +
        '<span>1. </span>' +

        '<span>Я</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>думаю</span>' +
        '<span data-t="m" data-c=", "> </span>' +

        '<span>ты</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>не</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>прав</span>' +
        '<span data-t="m" data-c=".">.</span>' +
    '</section>' +

    '<section data-role="text">' +
        '<span>2. </span>' +

        '<span>Москва</span>' +
        '<span data-t="m" data-c=" – "> </span>' +

        '<span>столица</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>России</span>' +
        '<span data-t="m" data-c=".">.</span>' +
    '</section>' +

    '<section data-role="text">' +
        '<span>3. </span>' +

        '<span>Ребята</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>разошлись</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>по</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>домам</span>' +
        '<span data-t="m" data-c=": "> </span>' +

        '<span>занятия</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>в</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>школе</span>' +
        '<span data-t="m" data-c=" "> </span>' +

        '<span>отменили</span>' +
        '<span data-t="m" data-c=".">.</span>' +
    '</section>' +

    '</task>' +

    '</component>';