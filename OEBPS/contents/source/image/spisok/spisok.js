$(function () {

	var 	$tasks = $(".spisok"),
		tasksNum = $tasks.length,
		$drop = $('.task-retry');

	$check.addClass('check_blocked');

	$tasks.each(function(){
		var		$input = $(this);

		$drop.click(function(){
			$input.val('').css("width", "").addClass('hold').removeClass('input_error').removeClass('input_ready');
			$check.addClass('check_blocked');
			//$drop.removeClass('showme_cell');
		});

		$input.on(MMO.settings.clickEvent, function(){
			if($(this).val() != '') $drop.addClass('showme_cell');
			if($input.val !== '') $input.removeClass('hold');
			if(!$('.hold').length) $check.removeClass('check_blocked');
		});

		$(this).find(".spisok").each(function(){
			var maxLen=$(this).find("option").eq(0).text().length;
			$(this).find("option").each(function(){
				if ($(this).text().length>maxLen) maxLen=$(this).text().length
			});
			$(this).css({"width": maxLen*0.75+"em"})
		});

	});

	$check.click(function(){

		if(!$check.hasClass('check_blocked')){

			$tasks.each(function(){
				var val = $(this).val();
				if(val != '' && $(this).attr("name").indexOf(val) != -1) $(this).addClass('input_ready').removeClass('input_error');
				else $(this).addClass('input_error').removeClass('input_ready');
			});

			//$drop.removeClass('showme_cell');

			if($('.input_ready').length == tasksNum){
				MMO.task.result.show(true);
			}
			else {
				MMO.task.result.show(false);
			}
		}
	});

	$restart.click(function(){
		$tasks.val('').css("width", "").addClass('hold').removeClass('input_error').removeClass('input_ready');
		$tasks.parents(".interakt_zadanie").find('.drop').removeClass('showme_cell');
		$check.addClass('check_blocked');
	});

});



