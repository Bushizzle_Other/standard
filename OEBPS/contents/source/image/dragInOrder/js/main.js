$(function(){


    MMO.load.libs([['$.ui','core/scripts/libs/jquery-ui-1.9.2.custom.min.js'], ['$.fn.hyphenate', 'core/scripts/libs/jquery.hyphen.ru.min.js']]);
    MMO.load.xml(function(){
        $sandbox.append(xml);

        $header.append($sandbox.find('title').html());
        $text.append($sandbox.find('question').html());

        initTask();

        $sandbox.find('style').appendTo('body');
    });

    function initTask(){

        $check.click(checkTask);

        $restart.click(restartTask);

        $retry.remove();


        //

        var $objs = $sandbox.find('objects'),
            vars = [], counter = 0;

        $objs.find('object').each(function(){

            vars.push({
                img: $(this).attr('picture'),
                txt: $(this).html(),
                ind: counter
            });

            counter++;
        });

        var $dragArea = $('<div/>').addClass('DIO__area');

        vars = MMO.helpers.shuffle(vars);

        vars.forEach(function(item){

            if(item.img){

                $dragArea.append(
                    $('<div/>')
                        .addClass('DIO__item')
                        .attr('data-val', item.ind)
                        .append(
                        $('<img/>')
                            .on(MMO.settings.clickEvent, function(){
                                MMO.plugins.showFullscreenImage(config.contentFolder + item.img, item.txt);
                            })
                            .addClass('DIO__item-img')
                            .attr('src', config.contentFolder + item.img),
                        $('<div/>')
                            .addClass('DIO__item-txt')
                            .append(item.txt.split(' ')[0]),
                        $('<div/>')
                            .addClass('DIO__item-zoom')
                            .on(MMO.settings.clickEvent, function(){
                                MMO.plugins.showFullscreenImage(config.contentFolder + item.img, item.txt);
                            })
                    )
                );


            } else {

                $dragArea.append(
                    $('<div/>')
                        .addClass('DIO__item DIO__item_textitem')
                        .attr('data-val', item.ind)
                        .append(
                        $('<div/>')
                            .addClass('DIO__item-txt')
                            .append(item.txt)
                    )
                );

                var itemsResizing = false,
                    resizeItems = function(){

                        if(!itemsResizing){

                            var maxSize = 0;

                            $('.DIO__item')
                                .css('height', 'auto')
                                .each(function(){
                                    if($(this).outerHeight() > maxSize)maxSize= $(this).outerHeight();
                                });

                            setTimeout(function(){
                                $('.DIO__item').css('height', maxSize + 'px');
                                itemsResizing = false;
                            }, 200);

                        }

                    };

                setTimeout(function(){
                    $('.DIO__item .DIO__item-txt').hyphenate();
                    resizeItems();
                }, 300);


                $(window).on('resize', resizeItems);

            }

        });

        $area.append($dragArea.sortable({
            distance: 50,
            tolerance: "pointer",
            axis: "x"
        }));

        $('.DIO__item')
            .css({
                width: 100/vars.length - 0.5 + '%',
                marginRight: '0.5%'
            });

        $('.DIO__item-txt').hyphenate();
    }

    function checkTask(){

        var errors = false;

        $('.DIO__item').each(function(){

            if($(this).index() != $(this).attr('data-val')){
                errors = true;
                $(this).addClass('DIO__error');
            }

        });

        if(errors){

            MMO.task.result.show(false);

        } else {

            MMO.task.result.show(true);

        }

    }

    function restartTask(){

        $('.DIO__error').removeClass('DIO__error');
    }

});