$(function(){

    var vars = [], $drags, $drops;

    MMO.load.libs([['$.ui','core/scripts/libs/jquery-ui-1.9.2.custom.min.js']]);

    MMO.load.xml(function(){

        $sandbox.append(xml);

        $header.append($sandbox.find('title').html());
        $text.append($sandbox.find('question').html());

        initTask();

        $sandbox.find('style').appendTo('body');

    });

    function initTask(){

        var $answerGroups = $sandbox.find('cell');

        $answerGroups.each(function(){

            var group = {
                name: $(this).find('title').html(),
                els: []
            };

            $(this).find('object').each(function(){
                group.els.push({
                    img: config.contentFolder + $(this).attr('picture'),
                    txt: $(this).html(),
                    val: group.name
                });
            });

            vars.push(group);


        });

        var allElements = [];

        vars.forEach(function(item){
            allElements = allElements.concat(item.els);
        });

        // init drags

        var $csArea =  $('<div/>').addClass('CS__area'),
            $csDrags = $('<div/>').addClass('CS__drags CS__parent'),
            $csDrops = $('<div/>').addClass('CS__drops');

        $csDrags.append(
            $('<div/>').addClass('CS__drags-window CS__window'),
            $('<div/>')
                .addClass('CS__drags-controls')
                .append(
                    $('<div/>')
                        .addClass('CS__drags-left')
                        .click(function(){
                            toggleSlide(-1, $(this));
                        }),
                    $('<div/>')
                        .addClass('CS__drags-right')
                        .click(function(){
                            toggleSlide(1, $(this));
                        })
                )
        );

        $area.append($csArea.append($csDrags, $csDrops));

        allElements = MMO.helpers.shuffle(allElements);

        allElements.forEach(function(item){

            var $elText = $('<div/>').addClass('drag-txt').append(item.txt);

            if($elText.html().length > 120){
                $elText.html($elText.html().substr(0,80) + '...')
            }

            var $el = $('<div/>')
                .addClass('CS__drags-item')
                .attr('data-val', item.val)
                .attr('id', 'drag-' + Math.round(Math.random()*100000))
                .append(
                    $('<img/>')
                        .attr('data-img', item.img)
                        .attr('data-txt', item.txt)
                        .on(MMO.settings.clickEvent, function(){
                            MMO.plugins.showFullscreenImage($(this).attr('data-img'), $(this).attr('data-txt'));
                        })
                        .addClass('drag-img')
                        .attr('src', item.img),
                    $elText,
                $('<div/>')
                    .addClass('drag-zoom')
                    .attr('data-img', item.img)
                    .attr('data-txt', item.txt)
                    .on(MMO.settings.clickEvent, function(){
                        MMO.plugins.showFullscreenImage($(this).attr('data-img'), $(this).attr('data-txt'));
                    })
                );



            $('.CS__drags-window').append($el);
        });

        $drags = $('.CS__drags-item');

        $drags.draggable({
            cursorAt: {
                left: 150,
                top: 150
            },
            helper: function() {
                return $(this).clone().width(300).height(300).css({zIndex: 1000, opacity:.6});
            },
            start: function(event, ui){

            },
            stop: function(event, ui) {

            }
        });

        $drags.eq(0).addClass('active');

        // init drops

        vars.forEach(function(item){

            $csDrops.append(
                $('<div/>')
                    .addClass('CS__drops-item CS__parent')
                    .append(
                    $('<div/>')
                        .addClass('drops-item__window CS__window')
                        .attr('data-val', item.name),
                    $('<div/>')
                        .addClass('drops-item__controls')
                        .append(
                        $('<div/>')
                            .addClass('drops-item__controls-left')
                            .click(function(){
                                toggleSlide(-1, $(this));
                            }),
                        $('<div/>')
                            .addClass('drops-item__controls-right')
                            .click(function(){
                                toggleSlide(1, $(this));
                            }),
                        $('<div/>')
                            .addClass('drops-item__controls-text')
                            .append(item.name)
                    )
                )
            );

        });

        $drops = $('.CS__drops-item');

        $drops.droppable({
            drop: function(event, ui){
                var $dragging = $(ui.draggable).clone(),
                    id = $dragging.attr('id'),
                    $ancestor = $('#' + id);

                $dragging
                    .draggable({
                        cursorAt: {
                            left: 150,
                            top: 150
                        },
                        helper: function() {
                            return $(this).clone().width(300).height(300).css({zIndex: 1000, opacity:.6});
                        }
                    })
                    .css('z-index', 999)
                    .find('.drag-img,.drag-zoom').on(MMO.settings.clickEvent, function(){
                        MMO.plugins.showFullscreenImage($(this).attr('data-img'), $(this).attr('data-txt'));
                    });

                toggleSlide(-1, $ancestor, 'rm');

                $(this).find('.active').removeClass('active');

                $(this)
                    .removeClass('hover')
                    .find('.CS__window')
                    .append($dragging);
            },
            over: function(){
                $(this).addClass('hover');
            },
            out: function() {
                $(this).removeClass('hover');
            }
        });

    }

    function toggleSlide(dir, $el, rm){

        var $slider = $el.closest('.CS__parent').find('.CS__window');

        if(rm)$el.remove();

        var $slides = $slider.children();

        if($slides.length){

            var $active = $slider.find('.active'),
                index = $active.index();

            var next = index + dir;

            if(next < 0)next = $slides.length - 1;
            if(next > $slides.length - 1) next = 0;

            $active.removeClass('active');
            $slides.eq(next).addClass('active');

        }
    }

    function checkTask(){

        var errors = false;

        $('.CS__drags-item').each(function(){
            var val = $(this).attr('data-val'),
                correct = $(this).closest('.CS__window').attr('data-val');

            if(val != correct){
                $(this).addClass('error');
                errors = true;
            }
        });

        if(errors) MMO.task.result.show(false);
        else MMO.task.result.show(true);

    }

    function restartTask(){

        location.reload();

    }

    $check.click(checkTask);
    $restart.click(restartTask);
    $retry.click(restartTask);

});