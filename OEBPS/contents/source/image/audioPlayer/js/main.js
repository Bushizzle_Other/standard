$(function(){

    $check.add($restart).add($retry).add($text).add($header).remove();

    var src;

    if(config.content.indexOf('.mp3') != -1){

        src = config.contentFolder + config.content.split('/')[1];
        audioPreload(src);
        MMO.task.afterInit();

    } else {

        MMO.load.xml(function(){
            $sandbox.append(xml);
            src = config.contentFolder + $sandbox.find('audio').text() + '.mp3';
            audioPreload(src);
        });

    }

    function audioPreload(src){
        var audio = document.createElement('audio'),
            percent = ['0.05','0.10','0.15','0.20','0.25','0.30','0.35','0.40', '0.45','0.50','0.55','0.60','0.65','0.70','0.75','0.80','0.85','0.90','0.95'],
            gradeReady = false;

        audio.ontimeupdate = function(){

            if(!gradeReady){
                var currentProgress = (audio.currentTime/audio.duration).toFixed(2);

                if(percent.includes(currentProgress)) {
                    percent.splice(percent.indexOf(currentProgress), 1);
                }

                if(!percent.length) {
                    MMO.task.result.show(true, 100, true);
                    gradeReady = true;
                }
            } else {
                audio.ontimeupdate = null;
            }
        };

        audio.src = src;
        audio.autoplay = true;
        audio.controls = true;
        $area.append(audio);
        $(window).trigger('resize');
    }

});