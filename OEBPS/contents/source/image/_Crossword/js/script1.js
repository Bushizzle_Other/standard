(function($) {
	$(function() {
		var puzzleData = [

				{
					clue: "3. Неопределённая форма возвратного глагола, имеющего одинаковый корень со словом <i>ласковый</i>.",
					answer: "ласкаться",
					position: 3,
					orientation: "across",
					startx: 1,
					starty: 2
				},
				{
					clue: "5. Неопределённая форма глагола со значением движения, которая без буквы <i>т</i> совпадает по написанию с повелительным наклонением этого же глагола.",
					answer: "идти",
					position: 5,
					orientation: "across",
					startx: 8,
					starty: 5,
				},
				{
					clue: "6. Повелительное наклонение глагола толочь.",
					answer: "толки",
					position: 6,
					orientation: "across",
					startx: 5,
					starty: 7
				},
				{
					clue: "9. Слово, которое обозначает действие или состояние и отвечает на вопросы: <i>что делать? что сделать?</i>",
					answer: "глагол",
					position: 9,
					orientation: "across",
					startx: 7,
					starty: 9
				},
				{
					clue: "10. Окончание глагола колоть в 3-м лице множественного числа настоящего времени.",
					answer: "ют",
					position: 10,
					orientation: "across",
					startx: 10,
					starty: 14
				},

				{
					clue: "1. Глагол <i>таять</i> в единственном числе прошедшего времени.",
					answer: "таял",
					position: 1,
					orientation: "down",
					startx: 2,
					starty: 1
				},
				{
					clue: "2. Окончание глагола <i>видеть</i> в 3-м лице единственного числа настоящего времени.",
					answer: "ит",
					position: 2,
					orientation: "down",
					startx: 6,
					starty: 1
				},
			 	{
					clue: "4. Непереходный глагол в 1-м лице единственного числа, образованный от существительного <i>ябеда</i>.",
					answer: "ябедничаю",
					position: 4,
					orientation: "down",
					startx: 9,
					starty: 2
				},
				{
					clue: "6. Переходный глагол I спряжения, 1-го лица множественного числа, совпадающий по написанию с количественным числительным в дательном падеже.",
					answer: "трём",
					position: 6,
					orientation: "down",
					startx: 5,
					starty: 7
				},
				{
					clue: "7. Повелительное наклонение глагола, оканчивающееся на согласную, но не имеющего на конце <i>ь</i>.",
					answer: "ляг",
					position: 7,
					orientation: "down",
					startx: 7,
					starty: 7
				},
				{
					clue: "8. Безличный глагол, обозначающий явление природы.",
					answer: "морозит",
					position: 8,
					orientation: "down",
					startx: 11,
					starty: 8
				}

			];

		$('#puzzle-wrapper').crossword(puzzleData);
		
	})
	
})(jQuery);
