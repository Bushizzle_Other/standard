(function($) {
	$(function() {
		var puzzleData = [
				{
					clue: "5. Вид спорта, включающий физические упражнения с отягощениями для развития мускулатуры.",
					answer: "бодибилдинг",
					position: 5,
					orientation: "across",
					startx: 6,
					starty: 5
				},
				{
					clue: "7. Система упражнений для развития гибкости.",
					answer: "стретчинг",
					position: 7,
					orientation: "across",
					startx: 1,
					starty: 7,
				},
				{
					clue: "8. Бывают  параллельные и разновысокие.",
					answer: "брусья",
					position: 8,
					orientation: "across",
					startx: 13,
					starty: 8
				},
				
				{
					clue: "11. Командная игра со стрельбой из специальных маркеров пневматического действия.",
					answer: "пейнтбол",
					position: 11,
					orientation: "across",
					startx: 2,
					starty: 11
				},
				{
					clue: "12. Вид спорта, состоящий из бега на лыжах и стрельбы?",
					answer: "биатлон",
					position: 12,
					orientation: "across",
					startx: 11,
					starty: 14
				},
			 	{
					clue: "1. Спортивная игра в шары, которая произошла от игры в кегли.",
					answer: "боулинг",
					position: 1,
					orientation: "down",
					startx: 9,
					starty: 1
				},
			 	{
					clue: "2. Один из приёмов самомассажа.",
					answer: "разминание",
					position: 2,
					orientation: "down",
					startx: 11,
					starty: 1
				},
			 	{
					clue: "3. Документ, в котором записаны основные правила организации и проведения Олимпийских игр.",
					answer: "хартия",
					position: 3,
					orientation: "down",
					startx: 18,
					starty: 3
				},
			 	{
					clue: "4. Дополнительный пятиминутный тайм в баскетболе.",
					answer: "овертайм",
					position: 4,
					orientation: "down",
					startx: 4,
					starty: 5
				},
			 	{
					clue: "6. Ведение, бросок, передача, подбор и перехват одним словом.",
					answer: "дриблинг",
					position: 6,
					orientation: "down",
					startx: 13,
					starty: 5
				},
			 	{
					clue: "9. Уличный баскетбол.",
					answer: "стритбол",
					position: 9,
					orientation: "down",
					startx: 16,
					starty: 8
				},
			 	{
					clue: "10. Нарушение, вызванное физическим контактом или неспортивным поведением игроков.",
					answer: "фол",
					position: 10,
					orientation: "down",
					startx: 8,
					starty: 10
				},
			 	{
					clue: "11. Один из самых полезных видов двигательной деятельности, который практически не имеет противопоказаний.",
					answer: "плавание",
					position: 11,
					orientation: "down",
					startx: 2,
					starty: 11
				}

				]

		$('#puzzle-wrapper').crossword(puzzleData);
		
	})
	
})(jQuery)
