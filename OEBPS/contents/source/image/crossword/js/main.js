/**
 * Jesse Weisbeck's Crossword Puzzle (for all 3 people left who want to play them)
 * Modified by Nikita Ignatov, 25-03-15
 */

$(function(){
	$.fn.crossword = function(entryData) {


		var puzz = {}; // put data array in object literal to namespace it into safety
		puzz.data = entryData;

		// append clues markup after puzzle wrapper div
		// This should be moved into a configuration object
		this.after('<div id="puzzle-clues">' +
			'<div class="puzzle-clues-list puzzle-clues-left">' +
			'<h2>По горизонтали:</h2>' +
			'<ul id="across"></ul>' +
			'</div>' +
			'<div class="puzzle-clues-list puzzle-clues-right">' +
			'<h2>По вертикали:</h2><ul id="down"></ul>' +
			'</div>' +
			'</div>');

		// initialize some variables
		var puzzEl = this,
			clues = $('#puzzle-clues'),
			clueLiEls,
			coords,
			entryCount = puzz.data.length,
			entries = [],
			rows = [],
			cols = [],
			solved = [],
			alwaysSolved = [],
			tabindex,
			$actives,
			activePosition = 0,
			activeClueIndex = 0,
			currOri,
			targetInput,
			mode = 'interacting',
			solvedToggle = false,
			z = 0;

		var puzInit = {

			init: function() {
				currOri = 'across'; // app's init orientation could move to config object

				// Reorder the problems array ascending by POSITION
				puzz.data.sort(function(a,b) {
					return a.position - b.position;
				});

				//console.log(puzz.data);

				// Set keyup handlers for the 'entry' inputs that will be added presently
				puzzEl.delegate('input', 'keyup', function(e){
					mode = 'interacting';


					// need to figure out orientation up front, before we attempt to highlight an entry
					switch(e.which) {
						case 39:
						case 37:
							currOri = 'across';
							break;
						case 38:
						case 40:
							currOri = 'down';
							break;
						default:
							break;
					}

					if ( e.keyCode === 9) {
						return false;
					} else if (
						e.keyCode === 37 ||
						e.keyCode === 38 ||
						e.keyCode === 39 ||
						e.keyCode === 40 ||
						e.keyCode === 8 ||
						e.keyCode === 46 ) {



						if (e.keyCode === 8 || e.keyCode === 46) {
							currOri === 'across' ? nav.nextPrevNav(e, 37) : nav.nextPrevNav(e, 38);
						} else {
							nav.nextPrevNav(e);
						}

						e.preventDefault();
						return false;
					} else {

						//alert('input keyup: '+solvedToggle);

						puzInit.checkAnswer(e);

					}

					e.preventDefault();
					return false;
				});

				// tab navigation handler setup
				puzzEl.delegate('input', 'keydown', function(e) {

					if ( e.keyCode === 9) {

						mode = "setting ui";
						if (solvedToggle) solvedToggle = false;

						//puzInit.checkAnswer(e)
						nav.updateByEntry(e);

					} else {
						return true;
					}

					e.preventDefault();

				});

				// tab navigation handler setup
				puzzEl.delegate('input', 'click', function(e) {
					mode = "setting ui";
					if (solvedToggle) solvedToggle = false;

					//alert('input click: '+solvedToggle);

					nav.updateByEntry(e);
					e.preventDefault();

				});

				clues.delegate('.clue-hint', 'click', function(e) {

					var hinted = $actives.filter(function(){return $(this).hasClass('entry-hinted')}).length,
						possible = Math.floor($actives.length/ 3.3) || 1;


					if(hinted < possible){
						var $freeToHint = $actives.filter(function(){return (!$(this).hasClass('entry-hinted') && $(this).val() != $(this).attr('data-val'))}),
							randomLetter = Math.floor(Math.random()*$freeToHint.length);

						var $newHint = $freeToHint.eq(randomLetter);

						$newHint.val($newHint.attr('data-val')).addClass('entry-hinted');

						if(hinted + 1 == possible) $(this).hide();
					}

					e.stopPropagation();

				});

				clues.delegate('.zoom', 'click', function(e) {
					MMO.plugins.showFullscreenImage($(this).attr('src'), $(this).attr('data-txt'));
					//e.stopPropagation();
				});


				// click/tab clues 'navigation' handler setup
				clues.delegate('li', 'click', function(e) {
					mode = 'setting ui';

					if (!e.keyCode) {
						nav.updateByNav(e);
					}
					e.preventDefault();
				});


				// highlight the letter in selected 'light' - better ux than making user highlight letter with second action
				puzzEl.delegate('#puzzle', 'click', function(e) {
					$(e.target).focus();
					$(e.target).select();
				});

				// DELETE FOR BG
				puzInit.calcCoords();

				// Puzzle clues added to DOM in calcCoords(), so now immediately put mouse focus on first clue
				clueLiEls = $('#puzzle-clues').find('li');

				// DELETE FOR BG
				puzInit.buildTable();
				puzInit.buildEntries();


				// other


				$check.click(function(){

					$('.entry').find('input').each(function(){
						if($(this).val() != $(this).attr('data-val')){
							$(this).addClass('cross-error');
						}
					});

					MMO.task.result.show(util.checkSolved());
				});

				$restart.add($retry).click(util.restart);

			},

			/*
			 - Given beginning coordinates, calculate all coordinates for entries, puts them into entries array
			 - Builds clue markup and puts screen focus on the first one
			 */
			calcCoords: function() {
				/*
				 Calculate all puzzle entry coordinates, put into entries array
				 */
				for (var i = 0, p = entryCount; i < p; ++i) {
					// set up array of coordinates for each problem
					entries.push([]);

					for (var x=0, j = puzz.data[i].answer.length; x < j; ++x) {
						entries[i].push(x);
						coords = puzz.data[i].orientation === 'across' ? "" + puzz.data[i].startx++ + "," + puzz.data[i].starty + "" : "" + puzz.data[i].startx + "," + puzz.data[i].starty++ + "" ;
						entries[i][x] = coords;
					}

					// while we're in here, add clues to DOM!
					$('#' + puzz.data[i].orientation).append('<li tabindex="1" data-position="' + i + '"><span class="clue-hint" title="Подсказка">?</span><span class="clue-text">' + puzz.data[i].clue + '</span></li>');
				}

				// Calculate rows/cols by finding max coords of each entry, then picking the highest
				for (var i = 0, p = entryCount; i < p; ++i) {
					for (var x=0; x < entries[i].length; x++) {
						cols.push(entries[i][x].split(',')[0]);
						rows.push(entries[i][x].split(',')[1]);
					}
				}

				rows = Math.max.apply(Math, rows) + "";
				cols = Math.max.apply(Math, cols) + "";

			},

			/*
			 Build the table markup
			 - adds [data-coords] to each <td> cell
			 */
			buildTable: function() {
				var tbl = '';

				for (var i=1; i <= rows; ++i) {
					tbl+='<tr>';
					for (var x=1; x <= cols; ++x) {
						tbl+='<td data-coords="' + [x,i] + '"></td>';
					}
					tbl+='</tr>';
				}

				puzzEl.append('<table id="puzzle">' + tbl + '</table>');
			},

			/*
			 Builds entries into table
			 - Adds entry class(es) to <td> cells
			 - Adds tabindexes to <inputs>
			 */
			buildEntries: function() {
				var light,
					$groupedLights,
					hasOffset = false,
					globalOffset = 0,
					positionOffset = entryCount - puzz.data[puzz.data.length-1].position; // diff. between total ENTRIES and highest POSITIONS


				for (var x=0; x < entryCount; x++) {
					var letters = puzz.data[x].answer.split(''),
						isVisible = puzz.data[x].visible,
						color = puzz.data[x].color,
						randClass = '';

					if(color) {
						randClass = 'color' + Math.round(Math.random()*1000);
						$('body').append('<style>.entry.' + randClass + ' input {background:' + color + '!important;}</style>');
					}

					if(isVisible) {
						solved.push(puzz.data[x].answer);
						alwaysSolved.push(puzz.data[x].answer);
					}

					if(x > 1 ){
						if (puzz.data[x].position === puzz.data[x-1].position) {
							hasOffset = true;
						}
					}

					for (var i=0; i < entries[x].length; i++) {
						light = $('td[data-coords="' + entries[x][i] + '"]');


						var visibleLetter = $(light).find('input').val();

						if($(light).empty()){

							$(light)
								.addClass('entry entry-' + (hasOffset ? x - positionOffset : x) + ' position-' + (x) )
								.append('<input maxlength="1" value="' + ((isVisible) ? letters[i] : (visibleLetter ? visibleLetter : '')) + '" data-val="' + letters[i] + '" type="text" tabindex="-1" />');

							if(i==0){
								hasOffset = false;
								$(light).append('<span>' + (puzz.data[x].position - globalOffset) + '</span>');
								if(hasOffset)globalOffset++;
							}

							if(color) $(light).addClass(randClass);
							if(isVisible) $(light).addClass('entry-visible');
						}
					}

				}

				util.highlightEntry();
				util.highlightClue();

			},


			/*
			 - Checks current entry input group value against answer
			 - If not complete, auto-selects next input for user
			 */
			checkAnswer: function(e) {

				var valToCheck, currVal;

				util.getActivePositionFromClassGroup($(e.target));

				valToCheck = puzz.data[activePosition].answer.toLowerCase();

				currVal = $('.position-' + activePosition + ' input')
					.map(function() {
						return $(this)
							.val()
							.toLowerCase();
					})
					.get()
					.join('');

				////alert(currVal + " " + valToCheck);
				if(valToCheck === currVal){
					$('.active')
						.addClass('entry-done');

					//$('.clues-active').addClass('clue-done');

					if(solved.indexOf(valToCheck) == -1){
						solved.push(valToCheck);
						console.log(solved);
					}
					solvedToggle = true;

					return;
				}

				currOri === 'across' ? nav.nextPrevNav(e, 39) : nav.nextPrevNav(e, 40);

				//z++;
				////alert(z);
				////alert('checkAnswer() solvedToggle: '+solvedToggle);

			}


		}; // end puzInit object


		var nav = {

			nextPrevNav: function(e, override) {

				var len = $actives.length,
					struck = override ? override : e.which,
					el = $(e.target),
					p = el.parent(),
					ps = el.parents(),
					selector;

				util.getActivePositionFromClassGroup(el);
				util.highlightEntry();
				util.highlightClue();

				$('.current').removeClass('current');

				selector = '.position-' + activePosition + ' input';

				////alert('nextPrevNav activePosition & struck: '+ activePosition + ' '+struck);

				// move input focus/select to 'next' input
				switch(struck) {
					case 39:
						p
							.next()
							.find('input')
							.addClass('current')
							.select();

						break;

					case 37:
						p
							.prev()
							.find('input')
							.addClass('current')
							.select();

						break;

					case 40:
						ps
							.next('tr')
							.find(selector)
							.addClass('current')
							.select();

						break;

					case 38:
						ps
							.prev('tr')
							.find(selector)
							.addClass('current')
							.select();

						break;

					default:
						break;
				}

			},

			updateByNav: function(e) {

				$('.clues-active, .active, .current').removeClass('clues-active active current');

				activePosition = ($(e.target).is( "li" )) ? $(e.target).data('position') : $(e.target).closest('li').data('position');

				util.highlightEntry();
				util.highlightClue();

				// store orientation for 'smart' auto-selecting next input
				currOri = $('.clues-active').parent('ol').attr('id');

				activeClueIndex = $(clueLiEls).index(e.target);
				////alert('updateByNav() activeClueIndex: '+activeClueIndex);

			},

			// Sets activePosition var and adds active class to current entry
			updateByEntry: function(e, next) {

				if(e.keyCode === 9 || next){
					// handle tabbing through problems, which keys off clues and requires different handling
					activeClueIndex = activeClueIndex === clueLiEls.length-1 ? 0 : ++activeClueIndex;

					$('.clues-active').removeClass('.clues-active');

					next = $(clueLiEls[activeClueIndex]);
					currOri = next.parent().attr('id');
					activePosition = $(next).data('position');

					// skips over already-solved problems
					util.getSkips(activeClueIndex);
					activePosition = $(clueLiEls[activeClueIndex]).data('position');


				} else {

					util.getActivePositionFromClassGroup(e.target);

					var clue = $('li[data-position=' + activePosition + ']');
					activeClueIndex = $(clueLiEls).index(clue);

					currOri = clue.parent().attr('id');

				}

				util.highlightEntry();
				util.highlightClue();

				//$actives.eq(0).addClass('current');
				////alert('nav.updateByEntry() reports activePosition as: '+activePosition);
			}

		}; // end nav object


		var util = {
			highlightEntry: function() {
				// this routine needs to be smarter because it doesn't need to fire every time, only
				// when activePosition changes
				$('.active').removeClass('active');
				$actives = $('.position-' + activePosition + ' input').addClass('active');
				$actives.eq(0).focus();
			},

			highlightClue: function() {
				var clue,
					$activeClue = $('li[data-position=' + activePosition + ']');

				$('.clues-active').removeClass('clues-active');
				$activeClue.addClass('clues-active');

				//$activeClue.find('.clue-hint').hide();

				if (mode === 'interacting') {
					clue = $activeClue;
					activeClueIndex = $(clueLiEls).index(clue);
				}
			},

			getClasses: function(light, type) {
				if (!light.length) return false;

				var classes = $(light).attr('class').split(' '),
					classLen = classes.length,
					positions = [];

				// pluck out just the position classes
				for(var i=0; i < classLen; ++i){
					if (!classes[i].indexOf(type) ) {
						positions.push(classes[i]);
					}
				}

				return positions;
			},

			getActivePositionFromClassGroup: function(el){

				var classes = util.getClasses($(el).parent(), 'position'),
					e1Ori, e2Ori, e1Cell, e2Cell;

				if(classes.length > 1){
					// get orientation for each reported position
					e1Ori = $('li[data-position=' + classes[0].split('-')[1] + ']').parent().attr('id');
					e2Ori = $('li[data-position=' + classes[1].split('-')[1] + ']').parent().attr('id');

					// test if clicked input is first in series. If so, and it intersects with
					// entry of opposite orientation, switch to select this one instead
					e1Cell = $('.position-' + classes[0].split('-')[1] + ' input').index(el);
					e2Cell = $('.position-' + classes[1].split('-')[1] + ' input').index(el);

					if(mode === "setting ui"){
						currOri = e1Cell === 0 ? e1Ori : e2Ori; // change orientation if cell clicked was first in a entry of opposite direction
					}

					if(e1Ori === currOri){
						activePosition = classes[0].split('-')[1];
					} else if(e2Ori === currOri){
						activePosition = classes[1].split('-')[1];
					}
				} else {
					activePosition = classes[0].split('-')[1];
				}

				//alert('getActivePositionFromClassGroup activePosition: '+activePosition);

			},

			checkSolved: function() {
				return solved.length == entryCount;
			},

			restart: function(){
				solvedToggle = false;

				$('#puzzle').find('.entry').each(function(){

					var $inp = $(this).find('input'), newVal = '';

					if($(this).hasClass('entry-visible') || $inp.hasClass('entry-hinted')) newVal = $inp.attr('data-val');

					$inp.val(newVal);

				});

				$('.entry-visible').val($(this).attr('data-val'));
				solved = alwaysSolved.slice();

				$('.cross-error').removeClass('cross-error');

				$('.active').eq(0).focus();
			},

			getSkips: function(position) {
				if ($(clueLiEls[position]).hasClass('clue-done')){
					activeClueIndex = position === clueLiEls.length-1 ? 0 : ++activeClueIndex;
					util.getSkips(activeClueIndex);
				} else {
					return false;
				}
			}

		}; // end util object


		puzInit.init();

	};

});

$(function(){

	if(isTask == 'regular'){
		MMO.load.xml(function(){
			var entryData = [];

			$sandbox.append(xml);

			$sandbox.find('word').each(function(i){

				var $el = $(this),
					$q = $el.find('question');

				entryData.push({
					clue: $q.attr('picture') ? '<img src="' + config.contentFolder + $q.attr('picture') +'"><div class="clue-zoom"></div>' : $q.html(),
					answer: $el.find('string').text(),
					position: i + 1,
					orientation: $el.attr('orientation'),
					startx: $el.attr('x') - 0,
					starty: $el.attr('y') - 0
					//visible: false,
					//color: '#fff'
				});

			});

			$('#stageContainer')
				.append($header.append($sandbox.find('title').text()))
				.append($('<div id="puzzle-container"><div id="puzzle-wrapper"></div></div>'));

			$('#puzzle-wrapper').crossword(entryData);

			$('.clue-zoom').click(function(){
				MMO.plugins.showFullscreenImage($(this).prev().attr('src'));
			});
		});
	}
});