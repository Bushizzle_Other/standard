String.prototype.hasChar = function(str){
	return (this.indexOf(str) != -1)
};

$(function(){

	var $component;

	var correctVars = [],
		wrongVars = [],
		allVars = [];

	var isHorizontal,
		isCopy,
		siblingsMode,
		multicellMode,
		siblingsPairs = [];

    var $dragsArea = $('<div/>').addClass('T__drags'),
        $dropsWrap = $('<div/>').addClass('T__drops-wrap');

	MMO.load.libs([['$.fn.hyphenate','core/scripts/libs/jquery.hyphen.ru.min.js'],['$.ui','core/scripts/libs/jquery-ui-1.9.2.custom.min.js']]);

	MMO.load.xml(function(){

		while(xml.indexOf('area') != -1){xml = xml.replace('area', 'box')}
		$sandbox.append(xml);

		$component = $sandbox.find('component');

		$header.append($component.find('title').html());
		$text.append($component.find('question').html());

		//

		isHorizontal = $component.attr('orientation') == 'horizontal';
		isCopy = $component.attr('mode') == 'copy';
		multicellMode = typeof $component.attr('multicell') != 'undefined';
		siblingsMode = typeof $component.attr('siblings') != 'undefined';

		if(isHorizontal)$area.addClass('T__horizontal');

		wrongVars = $sandbox.find('wrongWords');
		if(wrongVars.length) wrongVars = wrongVars.html().split('#');
		else wrongVars = [];


		initTask();

		$sandbox.find('style').appendTo('body');

	});

	function initTask(){

		$check.click(checkTask);
		$restart.click(restartTask);
		$retry.click(restartTask);

		//

        if(!isHorizontal) $dropsWrap.css('float', 'left');

		$sandbox.find('column').each(function(colNum){

			var $dropsArea = $('<div/>').addClass('T__drops'),
				itemRowCounter = 0;

			$(this).find('row').each(function(){

				var rowMarkup = $(this).html();

				if(rowMarkup.hasChar('#')){

					rowMarkup = rowMarkup.substr(1, rowMarkup.length-1).split('~');
					if(rowMarkup[rowMarkup.length-1] == '')rowMarkup.pop();

					rowMarkup.forEach(function(item){
						if(correctVars.indexOf(item) == -1)correctVars.push(item);
					});

					if(multicellMode){
						$dropsArea.append(
							$('<div/>')
								.attr('data-val', rowMarkup.join('#'))
								.addClass('T__drops-item T__drops-item_large')
								.droppable({
									accept: ".T__drags-item",
									drop: function(event, ui) {

										var $drop = $(this),
											$drag = $(ui.draggable).clone(),
											id = $drag.attr('id');

										$drag
											.attr('data-id', id)
											.attr('title', 'Удалить')
											.removeAttr('id')
											.click(function(){
												$('#' + id)
													.removeClass("T__disabled")
													.draggable("enable");
												$(this).remove();
												resizeTask();
											});

										if(!isCopy){
											$('#' + id)
												.addClass("T__disabled")
												.draggable("disable");
										}

										$drop
											.append($drag)
											.removeClass('T__hover');

										resizeTask();
									},
									over: function(){
										$(this).addClass('T__hover');
									},
									out: function() {
										$(this).removeClass('T__hover');
									}})
						);
						multicellMode = true;
						return false
					}
					else if(siblingsMode){

						$dropsArea.append(
							$('<div/>')
								.addClass('T__drops-item T__waiting')
								.attr('data-col', colNum)
								.attr('data-num', itemRowCounter)
								.droppable({
									accept: ".T__drags-item",
									drop: function(event, ui) {

										var $prevItem =  $(this).find('.T__drags-item');

										if($prevItem.length){
											if(!isCopy)$('#' + $prevItem.attr('data-id')).removeClass('T__disabled').draggable("enable");
											$prevItem.remove();
										}

										var $drop = $(this),
											$drag = $(ui.draggable).clone(),
											id = $drag.attr('id');

										var otherDropsAreaIndex = Math.abs(colNum - 1),
											$otherDrop = $('.T__drops').eq(otherDropsAreaIndex).find('.T__drops-item').eq($drop.attr('data-num'));

										$drag
											.attr('data-id', id)
											.attr('title', 'Удалить')
											.removeAttr('id');

										if($otherDrop.hasClass('T__waiting') && $drag.attr('data-col') == $drop.attr('data-col')){
											$otherDrop.attr('data-val', $drag.attr('data-pair'));
										}

										if(!isCopy){
											$('#' + id)
												.addClass("T__disabled")
												.draggable("disable");
										}

										$drag
											.click(function(){
												$('#' + id)
													.removeClass("T__disabled")
													.draggable("enable");
												$(this).remove();
												$drop.addClass('T__waiting');

												if(!$otherDrop.hasClass('T__waiting'))$otherDrop.attr('data-val', $otherDrop.find('.T__drags-item').attr('data-val'));
												else {
													$drop.removeAttr('data-val');
													$otherDrop.removeAttr('data-val');
												}


												resizeTask();
											});

										$drop
											.append($drag)
											.removeClass('T__hover T__waiting');


										if($otherDrop.hasClass('T__waiting')  && $drag.attr('data-col') == $drop.attr('data-col')){
											$drop.attr('data-val', $drag.attr('data-val'))
										}


										resizeTask();
									},
									over: function(){
										$(this).addClass('T__hover');
									},
									out: function() {
										$(this).removeClass('T__hover');
									}})
						);

						if(colNum > 0) siblingsPairs[itemRowCounter].push(rowMarkup[0]);
						else siblingsPairs.push([rowMarkup[0]]);
						itemRowCounter++;
					}
					else {
						$dropsArea
							.append(
							$('<div/>')
								.attr('data-val', rowMarkup.join('#'))
								.addClass('T__drops-item')
								.droppable({
									accept: ".T__drags-item",
									drop: function(event, ui) {

										var $prevItem =  $(this).find('.T__drags-item');

										if($prevItem.length){
											if(!isCopy)$('#' + $prevItem.attr('data-id')).removeClass('T__disabled').draggable("enable");
											$prevItem.remove();
										}

										var $drop = $(this),
											$drag = $(ui.draggable).clone(),
											id = $drag.attr('id');

										$drag
											.attr('data-id', id)
											.attr('title', 'Удалить')
											.removeAttr('id')
											.click(function(){
												$('#' + id)
													.removeClass("T__disabled")
													.draggable("enable");
												$(this).remove();
												resizeTask();
											});

										if(!isCopy){
											$('#' + id)
												.addClass("T__disabled")
												.draggable("disable");
										}

										$drop
											.append($drag)
											.removeClass('T__hover');

										resizeTask();
									},
									over: function(){
										$(this).addClass('T__hover');
									},
									out: function() {
										$(this).removeClass('T__hover');
									}})
						);
					}

				} else {

                    var $dropsTitle = $('<div/>').addClass('T__drops-title').append(rowMarkup);
                    $dropsTitle.hyphenate().appendTo($dropsArea);

				}

			});

            $dropsWrap.append($dropsArea);
		});

        $area.append($dropsWrap);

		if(siblingsMode || !multicellMode)$area.addClass('T__drops_cells');


		allVars = MMO.helpers.shuffle(allVars.concat(correctVars.concat(wrongVars)));

		initDrags();

        var $tDrops = $('.T__drops'),
            colsNum = $tDrops.length;

        $tDrops.css('width', 100 / colsNum + '%');

        if(!isHorizontal){

            var colWidth = 100/(colsNum + 1);

            $dropsWrap.css('width', colWidth*colsNum + '%');

            $dragsArea.css('width', colWidth + '%');

        }

		MMO.task.resizeHolder = resizeTask;
		resizeTask();

	}

	function getVarData(val){
		var res;

		siblingsPairs.forEach(function(item){
			var gotcha = item.indexOf(val);
			if(gotcha != -1) {
				res = [item[Math.abs(gotcha - 1)], gotcha];
				return 0;
			}
		});

		return res;
	}

	function initDrags(){

		allVars.forEach(function(item){

			if(item != ''){

				var $drag = $('<div class="T__drags-item" data-val="' + item + '" id="drag-' + Math.round(Math.random()*100000) + '">' + item + '</div>')
					.draggable({
						cursorAt: {
							left: 75,
							top: 10
						},
						helper: function() {
							return $(this).clone().width(150).css({zIndex: 100, opacity:.8});
						}
					});

				if(siblingsMode){
					var varData = getVarData(item);

					if(varData) $drag.attr('data-pair', varData[0]).attr('data-col', varData[1]);
					else alert('Задание оформлено неправильно, не хватает ответов');

				}

				$drag.hyphenate().appendTo($dragsArea);

			}

		});

		$area.append($dragsArea);
	}

	function restartTask(){
		$('.T__drags-item').remove();
		$('.T__error').removeClass('T__error');

		if(siblingsMode)$('.T__drops-item').removeAttr('data-val');

		initDrags();
	}

	function resizeTask(){
		var freeSpace = window.innerHeight - $header.outerHeight() - $text.outerHeight() - $('.cp-button-container').outerHeight();
		$('.task__area').css('height', freeSpace);

        freeSpace-=10;

		var $cellCols = $('.T__drops_cells').find('.T__drops'),
            $bigCells = $('.T__drops-item_large'),
            $tDrags = $('.T__drags');

        if(isHorizontal) freeSpace -= $tDrags.outerHeight();

		if($cellCols.length){

			$cellCols.eq(0).children().each(function(i){

				var siblingsArr = [],
					maxHeight = 0;

				$cellCols.each(function(){
					var $item = $(this).children().eq(i).css('height', 'auto');
					$item.find('.T__drags-item').css('height', 'auto');
					var itemHeight = $item.outerHeight();

					if(itemHeight > maxHeight) maxHeight = itemHeight;

					siblingsArr.push($item);

				});

				siblingsArr.forEach(function(item){
					$(item).height(maxHeight);
					$(item).find('.T__drags-item').height(maxHeight - 10);
				});



			});

		}

        $dropsWrap.css('height', freeSpace);
        if(!isHorizontal) $tDrags.css('height', freeSpace);

        if($bigCells.length){

            var maxColHeight = 0;

            $bigCells.css('min-height', 0);

            $bigCells.parent()
                .css('min-height', 0)
                .each(function(){
                var thisHeight = $(this).outerHeight();
                if(thisHeight > maxColHeight) maxColHeight = thisHeight;
            });

            if(maxColHeight < freeSpace) maxColHeight = freeSpace;

            $bigCells
                .each(function(){
                var $parent = $(this).parent();
                $(this).css('min-height', maxColHeight - $parent.find('.T__drops-title').outerHeight() - 30);
            });

        }

	}

	function checkTask(){

		var errors = false;

		if(siblingsMode){

			$('.T__drops-item').each(function(){
				var correct = $(this).attr('data-val').split('#'),
					$item = $(this).find('.T__drags-item'),
					val = $item.attr('data-val');

				if(!correct.includes(val) || !correct) {
					$(this).addClass('T__error');
					errors = true;
				}
			});

		} else {

			$('.T__drops-item').each(function(){

				var correct = $(this).attr('data-val').split('#'),
					vals = [];

				$(this).find('.T__drags-item').each(function(){
					vals.push({
						$el: $(this),
						val: $(this).attr('data-val')
					});
				});

				if(!vals.length) errors = true;

				vals.forEach(function(item){
					if(correct.indexOf(item.val) == -1) {
						errors = true;
						item.$el.addClass('T__error');
					}
				});

				if(multicellMode && vals.length != correct.length) errors = true;
			});

		}

		MMO.task.result.show(!errors);

	}

});